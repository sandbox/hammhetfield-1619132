<?php

class VisitorController extends DrupalDefaultEntityController {
	public function create() {
		return (object) array(
			'user_id' => '',
      'ip_address' => '',
		);
	}

  public function save($visitor) {
    $transaction = db_transaction();

    try {
      $visitor->is_new = empty($visitor->visitor_id);

      if(empty($visitor->created)) {
        $visitor->created = REQUEST_TIME;
      }

      $visitor->changed = REQUEST_TIME;

      if($visitor->is_new) {
        drupal_write_record('djehouti_visitor', $visitor);
        $op = 'insert';
      }
      else {
        drupal_write_record('djehouti_visitor', $visitor, 'visitor_id');
        $op = 'update';
      }

      module_invoke_all('entity_'.$op, $visitor, 'djehouti_visitor');

      unset($visitor->is_new);

      db_ignore_slave();

      return $visitor;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('djehouti_visitor', $e, NULL, WATCHDOG_ERROR);
      return false;
    }
  }
}