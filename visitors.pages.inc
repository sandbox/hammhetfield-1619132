<?php

// Visitor Dashboard
function djehouti_page_visitors_dashboard() {
  $header = array(
    t('Visitor ID'),
    t('User ID'),
    t('First Page Viewed'),
    t('Last Page Viewed'),
  );

  $query = db_select('djehouti_visitor', 'v');
  $vids = $query
    ->fields('v', array('visitor_id'))
    ->orderBy('v.changed', 'DESC')
    ->execute()
    ->fetchCol();

  $visitors = visitor_load_multiple($vids);
  $rows = array();

  foreach($visitors as $visitor) {
    $row = array();

    $row[] = $visitor->visitor_id;
    $row[] = $visitor->user_id;
    $row[] = format_date($visitor->created);
    $row[] = format_date($visitor->changed);

    $rows[] = $row;
  }

  return theme('table', array('header' => $header, 'rows' => $rows));

  return t('Visitors Dashboard page');
}

// Add Visitor
function djehouti_page_visitors_add() {
  global $user;

  $visitor = entity_get_controller('djehouti_visitor')->create();

  drupal_set_title(t('Dynamic title'));

  return drupal_get_form('djehouti_visitors_add_form', $visitor);
}




// Forms
function djehouti_visitors_add_form($form, &$form_state, $visitor) {
	$form['#id'] = 'djehouti_visitor_add';

	$form['#visitor'] = $visitor;
	$form_state['visitor'] = $visitor;

	$form['user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('User ID'),
    '#default_value' => $visitor->user_id,
    '#weight' => -5,
    '#required' => TRUE,
	);

  $form['ip_address'] = array(
    '#type' => 'textfield',
    '#title' => t('IP Address'),
    '#default_value' => $visitor->ip_address,
    '#required' => FALSE,
  );

  $form['buttons'] = array(
    '#weight' => 100,
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#weight' => 5,
      '#submit' => array('djehouti_visitor_add_form_submit'),
    ),
  );

  if(!empty($visitor->visitor_id)) {
    $form['buttons']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('djehouti_visitor_delete_form_submit'),
    );
  }

  //$form['#validate'][] = 'djehouti_visitor_form_validate';

  return $form;
}




// Form submit functions
function djehouti_visitor_add_form_submit($form, &$form_state) {
  global $user;

  $visitor = $form_state['visitor'];

  $visitor->user_id = $form_state['values']['user_id'];
  $visitor->ip_address = $form_state['values']['ip_address'];

  visitor_save($visitor);

  drupal_set_message(t('Visitor Saved.'));

  $form_state['redirect'] = 'admin/djehouti/visitors';
}