<?php

/**
 * Implement hook_schema()
 */
function djehouti_schema() {
  $schema['djehouti_visitor'] = array(
    'description' => 'This table contains visitors logs',
    'fields' => array(
      'visitor_id' => array(
        'description' => 'Primary Key : unique identifier of a visitor',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'user_id' => array(
        'description' => 'ID of the Drupal user',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'ip_address' => array(
        'description' => 'IP address of the visitor',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'created' => array(
        'description' => 'UNIX timestamp when the visitor entry was created',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'changed' => array(
        'description' => 'UNIX timestamp when the visitor entry was updated for the last time',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'unique keys' => array(
      'visitor_id' => array('visitor_id'),
    ),
    'primary key' => array('visitor_id'),
  );

  $schema['djehouti_visit'] = array(
    'description' => 'This table contains visits logs',
    'fields' => array(
      'visit_id' => array(
        'description' => 'Primary Key : unique ID of the visit',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'visitor_id' => array(
        'description' => 'Unique ID of the visitor',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'UNIX timestamp when the session started',
       'type' => 'int',
       'not null' => TRUE,
       'default' => 0,
     ),
     'changed' => array(
       'description' => 'UNIX timestamp when the session was updated for the last time',
       'type' => 'int',
       'not null' => TRUE,
       'default' => 0,
     ),
   ),
    'unique keys' => array(
      'visit_id' => array('visit_id'),
    ),
    'primary key' => array('visit_id'),
  );

  $schema['djehouti_pageview'] = array(
    'description' => 'This table contains pageviews logs',
    'fields' => array(
      'pageview_id' => array(
        'description' => 'Primary Key : unique ID of the pageview',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'visitor_id' => array(
        'description' => 'Unique ID of the visitor requesting the page',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'visit_id' => array(
        'description' => 'Unique ID of the visit during which the page was requested',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'request_path' => array(
        'description' => 'The URI requested to the server',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '<front>',
      ),
      'created' => array(
        'description' => 'UNIX timestamp when the pageview was recorded',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'unique keys' => array(
      'pageview_id' => array('pageview_id'),
    ),
    'primary key' => array('pageview_id'),
  );

  return $schema;
}