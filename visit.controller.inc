<?php

class VisitController extends DrupalDefaultEntityController {
	public function create($visitor_id = NULL) {
		return (object) array(
			'visitor_id' => $visitor_id,
		);
	}

	public function save($visit) {
		$transaction = db_transaction();

		try {
			$visit->is_new = empty($visit->visit_id);

			if(empty($visit->created)) {
				$visit->created = REQUEST_TIME;
			}

			$visit->changed = REQUEST_TIME;

			if($visit->is_new) {
				drupal_write_record('djehouti_visit', $visit);
				$op = 'insert';
			}
			else {
				drupal_write_record('djehouti_visit', $visit, 'visit_id');
				$op = 'update';
			}

			module_invoke_all('entity_'.$op, $visit, 'djehouti_visitor');

			unset($visit->is_new);

			db_ignore_slave();

			return $visit;
		}
		catch (Exception $e) {
			$transaction->rollback();
      watchdog_exception('djehouti_visit', $e, NULL, WATCHDOG_ERROR);
      return false;
		}
	}
}