<?php

class PageviewController extends DrupalDefaultEntityController {
	public function create($visitor, $visit, $path) {
    if(!isset($path)) {
      $path = NULL;
    }
    elseif($path == '') {
      $path = '<front>';
    }
    
		return (object) array(
			'visitor_id' => $visitor,
			'visit_id' => $visit,
			'request_path' => $path,
		);
	}

  public function save($pageview) {
    $transaction = db_transaction();

    try {
      $pageview->is_new = empty($pageview->pageview_id);

      if(empty($pageview->created)) {
        $pageview->created = REQUEST_TIME;
      }

      if($pageview->is_new) {
        drupal_write_record('djehouti_pageview', $pageview);
        $op = 'insert';
      }
      else {
        drupal_write_record('djehouti_pageview', $pageview, 'pageview_id');
        $op = 'update';
      }

      module_invoke_all('entity_'.$op, $pageview, 'djehouti_pageview');

      db_ignore_slave();

      unset($pageview->is_new);

      return $pageview;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('djehouti_visit', $e, NULL, WATCHDOG_ERROR);
      return false;
    }
  }
}